import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accordian-example',
  templateUrl: './accordian-example.component.html',
  styleUrls: ['./accordian-example.component.css']
})
export class AccordianExampleComponent implements OnInit {

  data = [
    {
      header: "Test1",
      dataList: [
        {
          "name": "Souradip",
          "email" : "sou@gmail.com"
        },
        {
          "name": "Goutam",
          "email" : "goutam@gmail.com"
        },
      ]
    },
    {
      header: "Test2",
      dataList: [
        {
          "name": "Max",
          "email" : "max@gmail.com"
        }
      ]
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}

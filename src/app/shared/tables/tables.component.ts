import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {

  @Input() columns = [];

  @Input() rowData = [];

  @Input() backupRowData = [];

  defaultSort = true;
  search : string;


  constructor() { }

  ngOnInit(): void {
  }

  sortData(ev){
    console.log(ev
      )
    var sortOrder = 1;
    this.defaultSort = !this.defaultSort;
    if(this.defaultSort == false){
      sortOrder = -1;
    }
    this.rowData.sort(this.dynamicSort(ev['data']))
 
  }

  dynamicSort(property) {
    var sortOrder = 1;
  
    if(this.defaultSort == false){
      sortOrder = -1;
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
  }

  searchColumnData(searchData, coldata){
    console.log(searchData, coldata)
    const {data} = coldata;
      if(searchData){
        this.rowData = this.backupRowData.filter(el => el[data].includes(searchData));
      } else {
          this.rowData = [...this.backupRowData];
      }
  }

}

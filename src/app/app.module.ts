import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TablesComponent } from './shared/tables/tables.component';
import { TablesExampleComponent } from './example/tables-example/tables-example.component';
import { AccordianComponent } from './shared/accordian/accordian.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AccordianExampleComponent } from './example/accordian-example/accordian-example.component';


@NgModule({
  declarations: [
    AppComponent,
    TablesComponent,
    TablesExampleComponent,
    AccordianComponent,
    AccordianExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccordionModule,
    BrowserAnimationsModule,
    AccordionModule.forRoot(),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

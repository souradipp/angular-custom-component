import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tables-example',
  templateUrl: './tables-example.component.html',
  styleUrls: ['./tables-example.component.css']
})
export class TablesExampleComponent implements OnInit {

  columns = [
    {name:'Id',data:'id',isSearchable : false, isSortable:false},
    {name:'First Name',data:'firstName',isSearchable : true,isSortable:true},
    {name:'Last Name',data:'lastName',isSearchable : true,isSortable:true},
    {name:'email',data:'email',isSearchable : true,isSortable:true}
  ];

  rowData = [
    {id:'1',firstName:"Max", lastName:"Tyson", email:"max@gmail.com"},
    {id:'2',firstName:"Test2", lastName:"yammer", email:"test2@gmail.com"},
    {id:'3',firstName:"Yadav", lastName:"Dey", email:"yadav@gmail.com"},
    {id:'4',firstName:"Goutam", lastName:"Ghosh", email:"goutam@gmail.com"},
    {id:'5',firstName:"Test5", lastName:"Max", email:"test5@gmail.com"},
  ];

  columns2 = [
    {name:'Id',data:'id',isSearchable : false,isSortable:false},
    {name:'First Name',data:'firstName',isSearchable : false,isSortable:false},
    {name:'Last Name',data:'lastName',isSearchable : false,isSortable:true},
    {name:'email',data:'email',isSearchable : false,isSortable:true}
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
